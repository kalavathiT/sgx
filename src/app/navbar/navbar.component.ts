import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { AppService } from '../app.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  currentPath: any = '';
  userName: any = '';
  constructor(
    private router: Router,
    private aroute: ActivatedRoute,
    private appService: AppService
  ) {
    router.events.subscribe((el) => {
      if (el instanceof NavigationEnd) {
        this.currentPath = el["url"];
        let login = localStorage.getItem("sgxloggedin");
        let rdata = JSON.parse(login)
        this.userName = (login == null) ? '' : ('name' in rdata) ? rdata['name'] : '';
      }
    });
  }

  ngOnInit(): void {
  }

  alertPage(event) {
    this.currentPath = 'alert';
    event.preventDefault();
    let loggedIn = localStorage.getItem("sgxloggedin");
    let path = (loggedIn == null) ? '/login' : '/alert';
    this.router.navigate([path]);

  }

  signOut() {
    this.appService.loader("show");
    setTimeout(() => {
      this.appService.loader("hide");
      localStorage.removeItem("sgxloggedin");
      this.router.navigate(["/nifty"]);
    }, 100);
  }

}
