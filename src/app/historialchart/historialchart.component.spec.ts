import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistorialchartComponent } from './historialchart.component';

describe('HistorialchartComponent', () => {
  let component: HistorialchartComponent;
  let fixture: ComponentFixture<HistorialchartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistorialchartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistorialchartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
