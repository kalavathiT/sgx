import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { NavbarComponent } from './navbar/navbar.component';
import { NiftygraphComponent } from './niftygraph/niftygraph.component';
import { AlertComponent } from './alert/alert.component';

const routes: Routes = [
  {
    path: "",
    redirectTo: "nifty",
    pathMatch: "full"
  },
  { path: "login", component: LoginComponent },
  { path: "register", component: RegisterComponent },
  { path: "navbar", component: NavbarComponent },
  { path: "nifty", component: NiftygraphComponent },
  { path: "alert", component: AlertComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
