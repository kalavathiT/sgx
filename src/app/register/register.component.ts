import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GoogleLoginProvider, SocialAuthService } from 'angularx-social-login';
import { ToastrService } from 'ngx-toastr';
import { AppService } from '../app.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  loginForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private authService: SocialAuthService,
    private apiservice: AppService,
    private toaster:ToastrService

  ) {
    this.authService.authState.subscribe((user) => {
    });
  }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      confirmPassword: ['', [Validators.required]],
    }, {
      validator: MustMatch('password', 'confirmPassword')
    });
  }



  signInWithGOOGLE(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  register(){
    this.apiservice.loader('show')
    let obj = JSON.parse(JSON.stringify(this.loginForm.value));
    obj["role"]= "1"; 
    this.apiservice.postData('signup',obj).then(resp=>{
      if(resp['error']){
        this.apiservice.loader('hide')
        this.toaster.error(resp['message'])
      }else{
        this.apiservice.loader('hide')
           this.toaster.success(resp['message'])
      }
    })
  }

}

export function MustMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
      // return if another validator has already found an error on the matchingControl
      return;
    }

    // set error on matchingControl if validation fails
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }
  }
}
