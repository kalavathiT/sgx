import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';

import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexYAxis,
  ApexXAxis,
  ApexTitleSubtitle,
  ApexTooltip,
  ApexDataLabels,
  ApexFill,
  ApexMarkers,
} from "ng-apexcharts";

import { dataSeries } from "./data-series";
import { AppService } from '../app.service';
import { interval, Subscriber, Subscription } from 'rxjs';
import { Router } from '@angular/router';


export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  yaxis: ApexYAxis;
  title: ApexTitleSubtitle;
  tooltip: ApexTooltip;
  stroke: any; //ApexStroke
};



@Component({
  selector: 'app-niftygraph',
  templateUrl: './niftygraph.component.html',
  styleUrls: ['./niftygraph.component.scss']
})
export class NiftygraphComponent implements OnInit, OnDestroy {
  @ViewChild("chart") chart: ChartComponent;
  demo1TabIndex: any = 0;
  public chartOptions: Partial<ChartOptions> = {
    series: [
      {
        name: "candle",
        type: "candlestick",
        data: []
      }
    ],
    chart: {
      height: 350,
      type: "candlestick",
    },
    title: {
      text: "SGX-NIFTY LIVE",
      align: "left"
    },
    stroke: {
      width: [3, 1]
    },
    xaxis: {
      type: "datetime"
    },
  };
  tab_list = { 0: "onemindata", 1: "fivemindata", 2: "fifteendata" }

  public series1: ApexAxisChartSeries;
  public chart1: ApexChart;
  public dataLabels1: ApexDataLabels;
  public markers1: ApexMarkers;
  public title1: ApexTitleSubtitle;
  public fill1: ApexFill;
  public yaxis1: ApexYAxis;
  public xaxis1: ApexXAxis;
  public tooltip1: ApexTooltip;
  apiLink: any = 'onemindata'
  val = 1;
  intervalTab1: Subscription;
  currentData: any = {};
  constructor(private appService: AppService, private router: Router) {
    // this.initChartData();
  }

  ngOnInit(): void {
    this.firstTab(60000);
    // this.intervalTab1.unsubscribe();
  }

  firstTab(count) {
    this.getValue();
    this.intervalTab1 = interval(count).subscribe(x => {
      this.getValue();
    });
  }

  tabSelected(item) {
    this.apiLink = this.tab_list[item['index']];
    this.intervalTab1.unsubscribe();
    let count = item['index'] ? item['index'] == 1 ? 60000 : 60000 : 60000;
    this.firstTab(count);
  }


  getValue() {
    this.appService.getData('curentData').then(res => {
      this.currentData = res['data']
      this.currentData.prevClose = +(this.currentData.prevClose)
      this.currentData.prevHigh = +(this.currentData.prevHigh)
      this.currentData.prevLow = +(this.currentData.prevLow)
      this.currentData.prevTime = +(this.currentData.prevTime)
      this.currentData.prevOpen = +(this.currentData.prevOpen)
      this.currentData.todayClose = +(this.currentData.todayClose)
      this.currentData.todayHigh = +(this.currentData.todayHigh)
      this.currentData.todayLow = +(this.currentData.todayLow)
      this.currentData.todayOpen = +(this.currentData.todayOpen);
      let currentTime = new Date();


      var currentOffset = currentTime.getTimezoneOffset();

      var ISTOffset = 330;   // IST offset UTC +5:30 

      var ISTTime = new Date(currentTime.getTime() + (ISTOffset + currentOffset) * 60000);

      // ISTTime now represents the time in IST coordinates

      var hr = ISTTime.getHours()
      var min = ISTTime.getMinutes()
      var  sec = ISTTime.getSeconds()
      // let hr = String(date.getHours()).length < 2 ? `0${date.getHours()}` : `${date.getHours()}`;
      // let min = String(date.getMinutes()).length < 2 ? `0${date.getMinutes()}` : `${date.getMinutes()}`;
      // let sec = String(date.getSeconds()).length < 2 ? `0${date.getSeconds()}` : `${date.getSeconds()}`;
      this.currentData.todayTime = this.diff_minutes(hr, min, sec);
    })
    this.appService.getData(this.apiLink).then(resp => {
      if (resp['error']) {

      } else {
        this.reqFormatData(resp['data'].splice(0, 100), 'candlestick')
      }
    })
  }

  diff_minutes(hr, min, sec) {
    return `${hr}:${min}`;
  }

  reqFormatData(items, type) {
    let arr = [];
    for (let i = 0; i <= items.length - 1; i++) {
      let obj = items[i];
      let date = new Date(obj['time'] * 1000)
      arr.push(
        {
          x: date,
          y: [obj["open"], obj["high"], obj["low"], obj["close"]]
        }
      )
      if (i == items.length - 1)
        this.chartOptions['series'][0]['data'] = arr;
      this.chartOptions['series'][0]['type'] = type;
      this.chartOptions['chart']['type'] = type;
      this.chartOptions = JSON.parse(JSON.stringify(this.chartOptions))
    }
  }

  maintabSelected(event) {
    if (event['index'] == 0) {
      this.demo1TabIndex = 0;
      this.tabSelected(event);
    } else {
      this.intervalTab1.unsubscribe();
      this.chartOptions['series'][0]['data'] = [];
      this.gethistoricaldata();
    }
  }

  gethistoricaldata() {
    this.appService.getData('gethistoricaldata').then(resp => {
      if (resp['error']) {

      } else {
        this.chartOptions['series'][0]['type'] = 'area';
        this.chartOptions['chart']['type'] = 'area';
        this.reqFormatData(resp['data'].splice(0, 100), 'area')
      }
    })
  }

  alertPage(event) {
    event.preventDefault();
    let loggedIn = localStorage.getItem("sgxloggedin");
    let path = (loggedIn == null) ? '/login' : '/alert';
    this.router.navigate([path]);
  }

  ngOnDestroy() {
    this.intervalTab1.unsubscribe();
  }

}



