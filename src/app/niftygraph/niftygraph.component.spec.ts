import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NiftygraphComponent } from './niftygraph.component';

describe('NiftygraphComponent', () => {
  let component: NiftygraphComponent;
  let fixture: ComponentFixture<NiftygraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NiftygraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NiftygraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
