import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  apiURL = 'http://15.207.2.166:3000/api/';
  // apiURL ='http://localhost:3100/api/'
  constructor(private http: HttpClient) { }
  async postData(url, data) {
    return new Promise(async (resolve, reject) => {
      var sgxloggedin = await JSON.parse(localStorage.getItem('sgxloggedin'));
      let token = (sgxloggedin != null) ? sgxloggedin['token'] : '';
      let token2 = (sgxloggedin != null) ? sgxloggedin['refresh_token'] : '';
     
      let option = new HttpHeaders({ 'Content-Type': 'application/json', 'x-access-token': token,'X-Refresh-Token':token2 })
      this.http.post(this.apiURL + url, data, { headers: option }).subscribe(res => { resolve(res) },
        err => { reject(err) }
      )
    })
  }

  async getData(url) {
    return new Promise(async (resolve, reject) => {
      var sgxloggedin = await JSON.parse(localStorage.getItem('sgxloggedin'));
      let token = (sgxloggedin != null) ? sgxloggedin['token'] : '';
      let token2 = (sgxloggedin != null) ? sgxloggedin['refresh_token'] : '';
     
      let option = new HttpHeaders({ 'Content-Type': 'application/json', 'x-access-token': token,'X-Refresh-Token':token2 })
      this.http.get(this.apiURL + url, { headers: option }).subscribe(res => { resolve(res) },
        err => { reject(err) }
      )
    })

  }

  async putData(url, data) {
    return new Promise(async (resolve, reject) => {
      var sgxloggedin = await JSON.parse(localStorage.getItem('sgxloggedin'));
      let token = (sgxloggedin != null) ? sgxloggedin['token'] : '';
      let token2 = (sgxloggedin != null) ? sgxloggedin['refresh_token'] : '';
     
      let option = new HttpHeaders({ 'Content-Type': 'application/json', 'x-access-token': token,'X-Refresh-Token':token2 })
      this.http.put(this.apiURL + url, data, { headers: option }).subscribe(res => { resolve(res) },
        err => { reject(err) }
      )
    })
  }


  async deleteData(url) {
    return new Promise(async (resolve, reject) => {
      var sgxloggedin = await JSON.parse(localStorage.getItem('sgxloggedin'));
      let token = (sgxloggedin != null) ? sgxloggedin['token'] : '';
      let token2 = (sgxloggedin != null) ? sgxloggedin['refresh_token'] : '';
     
      let option = new HttpHeaders({ 'Content-Type': 'application/json', 'x-access-token': token,'X-Refresh-Token':token2 })
      this.http.delete(this.apiURL + url, { headers: option }).subscribe(res => { resolve(res) },
        err => { reject(err) }
      )
    })
  }

  loader(type){
    let loader = document.getElementById('loader')
     type== 'show' ? loader.classList.add("flex") : loader.classList.remove("flex")
  }

  
}
