import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AppService } from '../app.service';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {
  todayClose: any = 0;
  aletTable: any = [];
  conditionArr = [
    { id: "1", name: "Moves Above" },
    { id: "2", name: "Moves Below" },
    { id: "3", name: "Gain% is or Above" },
    { id: "4", name: "Loss% is or Above" }
  ]
  form: FormGroup;
  constructor(private appService: AppService, private fb: FormBuilder, private toaster: ToastrService) {
    this.form = this.fb.group({
      condition: ['1', Validators.required],
      price: ["", [Validators.required, Validators.min(1)]],
      frequency: [{ value: '1', disabled: true }, Validators.required]
    })
  }

  ngOnInit(): void {
    this.getValue();
    this.getAlertList();
  }

  getValue() {
    this.appService.loader('show')
    this.appService.getData('curentData').then(res => {
      if (res['error']) {
        this.appService.loader('hide')
      } else {
        this.appService.loader('hide')
        this.todayClose = +(res['data'].todayOpen);
        let count  = this.form.value['condition'] == '1' ? this.todayClose + 100 : this.todayClose - 100;
        this.form.controls.price.setValue(count);
      }
    })
  }

  conditionChange(data) {
    this.form.controls.frequency[["3", "4"].includes(data.value) ? 'enable' : 'disable']();
    ["3", "4"].includes(data.value) ? this.form.controls.price.setValue(1) : this.getValue();
    this.form.controls.price.clearValidators();
    if (["3", "4"].includes(data.value)) {
      this.form.controls.price.setValidators([Validators.required, Validators.min(1), Validators.max(10)])
    } else {
      this.form.controls.price.setValidators([Validators.required, Validators.min(1)])
    }
    this.form.controls.price.updateValueAndValidity();
  }

  create() {
    this.appService.loader('show')
    if(this.aletTable.length < 5){
      let obj = this.form.getRawValue();
      this.appService.postData('createAlert', obj).then(res => {
        if (res['error']) {
          this.appService.loader('hide')
          this.toaster.error(res['message'])
        } else {
          this.appService.loader('hide')
          this.toaster.success(res['message']);
          this.getAlertList();
        }
      })
    }else{
      this.toaster.warning("Maximum 5 alerts can be created");
    }

  }

  getAlertList() {
    this.appService.loader('show')
    this.appService.getData('getAlerts').then(res => {
      if (res['error']) {
        this.appService.loader('hide')
        this.toaster.error(res['message'])
      } else {
        this.appService.loader('hide')
        this.aletTable = this.formatChange(res["data"]);
      }
    })
  }

  formatChange(data) {
    return data.map((el, ind) => {
      let conName = this.conditionArr.filter(ele=>ele['id']==el['condition'])[0]['name'];
      return {
         id: el['_id']['$oid'],
         sno: ind+1,
         data:`SGX NIFTY ${conName} ${el.price}`
      }
    });
  }

  delete(id){
    this.appService.loader('show')
    let obj = {"alertId":id};
    this.appService.postData('deleteAlertById',obj).then(res => {
      if (res['error']) {
        this.appService.loader('hide')
        this.toaster.error(res['message'])
      } else {
        this.appService.loader('hide')
        this.toaster.success(res['message'])
        this.getAlertList();
      }
    })
  }

}
