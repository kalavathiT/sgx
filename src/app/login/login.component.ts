import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AppService } from '../app.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {


  loginForm: FormGroup;
  user: any;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private apiService: AppService,
    private toaster: ToastrService

  ) {

  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  loggedIn() {
    let obj = this.loginForm.value;
    this.apiService.loader('show')
    this.apiService.postData('login', obj).then(resp => {
      if (resp['error']) {
        this.toaster.error(resp['message']);
        this.apiService.loader('hide');
      } else {
        this.toaster.success("successfully loggedin");
        localStorage.setItem('sgxloggedin',JSON.stringify(resp['data']));
        setTimeout(() => {
          this.router.navigate(['/alert']);
          this.apiService.loader('hide');
        });
      }
    })
  }
}

